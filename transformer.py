import pandas as pd
from rdflib import Graph, Literal, Namespace, RDF, URIRef
from rdflib.namespace import XSD

# Function to read the dataset
def read_csv(file_path):
    try:
        data = pd.read_csv(file_path, sep=',')
        return data
    except FileNotFoundError:
        print(f"Error: The file {file_path} does not exist.")
        exit(1)
    except pd.errors.EmptyDataError:
        print(f"Error: The file {file_path} is empty.")
        exit(1)
    except pd.errors.ParserError:
        print(f"Error: The file {file_path} does not appear to be a valid CSV.")
        exit(1)

# Function to create the RDF graph
def create_rdf_graph(data):
    g = Graph()
    
    # Define Namespaces
    SAREF = Namespace("https://saref.etsi.org/core/")
    EX = Namespace("http://example.org/")
    
    g.bind("saref", SAREF)
    g.bind("ex", EX)

    # Iterate over each row in the dataset
    for idx, row in data.iterrows():
        timestamp = row['utc_timestamp']
        for col in data.columns:
            if col != 'utc_timestamp' and col != 'cet_cest_timestamp' and col != 'interpolated':
                value = row[col]
                if pd.notna(value):
                    measurement = URIRef(f"http://example.org/measurement/{idx}/{col}")
                    device = URIRef(f"http://example.org/device/{col}")
                    building = URIRef(f"http://example.org/building/{col.split('_')[2]}")  # Assuming the building ID is part of the column name
                    
                    g.add((measurement, RDF.type, SAREF.Measurement))
                    g.add((measurement, SAREF.hasValue, Literal(value, datatype=XSD.float)))
                    g.add((measurement, SAREF.isMeasuredBy, device))
                    g.add((measurement, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))
                    
                    g.add((device, RDF.type, SAREF.Device))
                    g.add((device, SAREF.isLocatedIn, building))

    return g

# Main function to execute the transformation
def main():
    # Hardcoded path to the CSV file
    file_path = sys.argv[1]
    
    # Read the dataset
    data = read_csv(file_path)
    
    # Create RDF graph
    rdf_graph = create_rdf_graph(data)
    
    # Serialize the RDF graph to a Turtle file
    rdf_graph.serialize(destination="graph.ttl", format="turtle")
    print("RDF graph has been serialized to graph.ttl")

if __name__ == "__main__":
    main()
